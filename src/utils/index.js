export const uiid = (text) => {

    // Remove parenthesis
    let cleanText = text.replace(/ *\([^)]*\) */g, "");
    
    cleanText = cleanText.replace(/[{()}]/g, '');
    cleanText = cleanText.replace(/ *\[[^\]]*]/, '');
    cleanText = cleanText.replace(/\./g,'')
    cleanText = cleanText.replace(/\,/g,'-')
    cleanText = cleanText.replace(/\'/g,'-')
    cleanText = cleanText.replace(/\"/g,'-')
    cleanText = cleanText.replace(/\*/g,'-')
    cleanText = cleanText.replace(/\ /g,'-')
    
    cleanText = cleanText.trim();
    cleanText = cleanText.toLowerCase();
    
    return cleanText

}


export const range = (start, end) => new Array(end - start + 1).fill(undefined).map((value, index) => index + start)