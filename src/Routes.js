import React, { Component } from 'react';

import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import Calculator from './components/Calculator';
import TestFormWrap from './components/TestFormWrap';

class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Calculator} />
                <Route path="/fields" component={TestFormWrap} />
                <Route path="*" component={Calculator} />
            </Switch>
        );
    }
}

export default Routes;