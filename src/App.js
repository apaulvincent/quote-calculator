import React from "react";
import { BrowserRouter } from 'react-router-dom';

import './App.scss';
import styled from 'styled-components'

import Routes from './Routes'

import { Provider } from 'react-redux';
import store from './store';


const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes/>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

