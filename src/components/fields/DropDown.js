import React from "react";
import styled from 'styled-components';
import * as utils from '../../utils';

import { connect, getIn } from 'formik';

import Select from 'react-select'

// create seperate component for async dropdown
// import AsyncSelect from 'react-select/lib/Async';
import InputFeedback from './InputFeedback'

const DropDown = ({label, name, visible, ...props}) => {

    if (!visible) return null;
    
    let uiid = utils.uiid(label);

    const error = getIn(props.formik.errors, name);
    const touch = getIn(props.formik.touched, name);

    let className = touch && error ? 'has-error' : '';
    let errorMsg = touch && error ? error : null;

    const value = props.formik.values[name];

    let options =  props.options.map((o, i) => {

        if( o.value ) {
            return o;
        }
            
        return {
            value: o,
            label: o
        }

    })

    // convert selected value to object for select component
    const selected = (value.value) ? value : {
        value: value,
        label: value,
    }

    const handleChange = value => {
        // this is going to call setFieldValue and manually update values
        props.formik.setFieldValue(name, value.value);
        // this is going to call setFieldTouched and manually update touched
        props.formik.setFieldTouched(name, true);

        // Emit onChange Event
        props.onChange(value); 

    };

    return (
        <Container className={className}>

        <Label htmlFor={uiid}><span>{label}</span></Label>

        <Select
            name={name} 
            options={options}
            value={selected}
            inputId={uiid}
            onChange={handleChange}
            isDisabled={false}
            isSearchable={false}
            isMulti={false}
        />

        <InputFeedback error={errorMsg}/>

        </Container>
    );
}

export default connect(DropDown)

DropDown.defaultProps = {
    visible: true,
    onChange: () => {},
    onBlur: () => {}
}

const Container = styled.div `
    margin: 0  0 10px;

    &.has-error {
      color: red;

      select {
        border: 1px solid red;
      }

    }

    & > select {
        display: block;
        width: 100%;
        border: 1px solid #ddd;
        padding: 5px 12px;
        border-radius: 4px;
    }

`

const Label = styled.label `
    display: block;
`