import React from "react";
import styled from 'styled-components';
import * as utils from '../../utils';

import { connect, getIn } from 'formik';
import InputFeedback from './InputFeedback'

const ButtonGroup = ({label, name, visible, ...props}) => {

    if (!visible) return null;

    const error = getIn(props.formik.errors, name);
    const touch = getIn(props.formik.touched, name);

    let className = touch && error ? 'has-error' : '';
    let errorMsg = touch && error ? error : null;
    const value = props.formik.values[name];

    const handleChange = e => {

      let value = e.nativeEvent.target.value;

      props.formik.setFieldValue(name, value);
      props.formik.setFieldTouched(name, true);
      props.onChange(value); 
  };

    return (
        <Container className={className}>
          <SubLabel>{label}</SubLabel>
          <div>
            {
              props.options.map( (o, i) => {
                
                let fieldLabel = o;
                let fieldValue = o;

                if(o.value) {
                  fieldLabel = o.label;
                  fieldValue = o.value;
                }

                let uiid = props.type + '-' + utils.uiid(fieldLabel) + `-${i}`;
                let checked = value === fieldValue;
                let labelClass =  checked ? 'checked' : '';

                return <Label htmlFor={uiid} key={i} className={labelClass}>
                            <span>{fieldLabel}</span>
                            <input id={uiid} type={props.type} name={name} value={fieldValue} checked={checked} onChange={handleChange}/>
                        </Label>
                })
            }
          </div>

          <InputFeedback error={errorMsg}/>
        </Container>
    );
}

export default connect(ButtonGroup)

ButtonGroup.defaultProps = {
  type: 'radio',
  visible: true,
  onChange: () => {},
}

const Container = styled.div `
    margin: 0  0 10px;

    &.has-error {
      color: red;

      label {
        border-color: red;
      }
    }
`

const SubLabel = styled.label `
`

const Label = styled.label `
    cursor: pointer;
    border: 1px solid #ddd;
    border-left: none;
    padding: 5px 12px;
    text-align: center;

    &.checked {
      background: #ddd;
    }

    & > input {
      display: none
    }

    &:first-child {
      border-radius: 4px 0 0 4px;
      border-left: 1px solid #ddd;
    }

    &:last-child {
      border-radius: 0 4px 4px 0;
    }
`