import React, { useEffect, useState } from "react";
import styled from 'styled-components';
import * as utils from '../../../utils';

import InputRange from 'react-input-range';

import { connect, getIn } from 'formik';
import InputFeedback from '../InputFeedback'

import './RangeSlider.scss';

const getSliderNodeWidth = (el) => {
    let slider = null;

    for (var i = 0; i < el.node.childNodes.length; i++) {

        const track = el.node.childNodes[i];

        if (track.className === 'input-range__track input-range__track--background') {

            for (var x = 0; x < track.childNodes.length; x++) {
            
                if (track.childNodes[x].className === 'input-range__slider-container') {
                    slider = track.childNodes[x];
                    break;
                }  
            }

            break;
        }  
    }

    return new Promise( (resolve, reject) => {

            if(slider) {
                resolve(Math.ceil(slider.offsetWidth / 2))
            } else {
                reject('Error, slider is NULL')
            }
    });
}

const  RangeSlider = ({ label, name, min, max, step, visible, formatLabel, disabled, extraData, ...props}) => {

    const [val, setValue] = useState(min);
    const [width, setWidth] = useState(30);

    if (!visible) return null;

    const error = getIn(props.formik.errors, name);
    const touch = getIn(props.formik.touched, name);

    let className = touch && error ? 'has-error' : '';
    let errorMsg = touch && error ? error : null;
    const value = props.formik.values[name];
    let uiid = utils.uiid(label);

    let slideTag = null

    useEffect(() => {
        
        let mounted = true

        if(mounted) {

            setValue(value)

            if(slideTag) {
    
                getSliderNodeWidth(slideTag).then(setWidth)
            }

        };

        // Do Cleanup!...
        return () => {
            slideTag = null;
            mounted = false;
        }

    }, [extraData]); // <-- Add [] so only re-run the effect if extraData changes

    const handleChange = (val) => {
        setValue(val)
    }

    const onChangeComplete = (val) => {
        props.formik.setFieldValue(name, val);
        props.formik.setFieldTouched(name, true);
        props.onChange(val); 
    }

    const node = e => {
        slideTag = e;
    }

    return (
        <Container className={`component__range-slider ${className}`}>
            <Label htmlFor={uiid}><span>{label}</span></Label>

            <InputRangeWrap style={{marginLeft: width, marginRight: width}}>
                <InputRange
                    formatLabel={formatLabel}
                    step={step}
                    minValue={min}
                    maxValue={max}
                    disabled={disabled}
                    value={val}
                    ref={node}
                    name={name}
                    draggableTrack={false}
                    onChangeComplete={onChangeComplete}
                    onChange={handleChange} />
            </InputRangeWrap>

            <InputFeedback error={errorMsg}/>
        </Container>
    );
}

export default connect(RangeSlider);

RangeSlider.defaultProps = {
    visible: true,
    disabled: false,
    extraData: false,
    label: 'Range',
    value: 1,
    min: 1,
    max: 100,
    name: 'range-slider', 
    step: 1, 
    onChange: () => {}, 
    error: null, 
    formatLabel: () => {}, 
}

const Container = styled.div `
    margin: 0  0 10px;
    overflow: hidden;
    display: relative;

    &.has-error {
      color: red;
    }
`

const InputRangeWrap = styled.div `
    padding: 0;
`

const Label = styled.label `
    display: block;
`