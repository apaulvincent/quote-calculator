import React from "react";
import styled from 'styled-components';

const InputFeedback = ({error})  => {
    return error ? <Container className="field-error">{error}</Container> : null;
} 

export default InputFeedback;

const Container = styled.div `
    font-size: 13px;
    background: red;
    padding: 3px 10px 5px;
    color: #fff;
    line-height: 1;
    display: inline-block;
    border-radius: 4px;
`