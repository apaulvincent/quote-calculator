import React, { useEffect, useState } from "react";
import styled from 'styled-components';

import * as utils from '../../../utils';
import DatePicker from "react-datepicker";
import moment from 'moment'

import InputFeedback from '../InputFeedback'

export default function DatePickerField({label, name, onChange, value, error, visible, ...props}) {

    if (!visible) return null;

    const [date, setDate] = useState();

    let uiid = label ? utils.uiid(label) : label;

    let className = error ? 'has-error' : '';

    let elem = null;
    let event = new Event('change');

    useEffect(() => {
        
        let mounted = true

        if(mounted) {
            if(elem) {
                elem.addEventListener('change', onChange)
            }
        }

        return () => {
            elem = null;
            mounted = false
        }

    }, []); // <-- Add [] so it only runs on mount and unmount

    const dateInput = e => {
        elem = e;
    }

    const handleChange = (date) => {

        setDate(date)

        elem.value = moment(date).format('MM/DD/YYYY')
        elem.dispatchEvent(event)
    }

    return (
        <Container className={className}>
                
            <Label htmlFor={uiid}><span>{label}</span></Label>
            <DatePicker
                id={uiid}
                selected={date}
                name={name}
                onChange={handleChange}
                // dateFormat="MM/DD/YYYY"
                customInput={<CustomInput dateInput={dateInput} />}
            />
            <InputFeedback error={error}/>

        </Container>
    );
}


class CustomInput extends React.Component {
    render () {
      return (
          <CustomInputWrap>
            <input name={this.props.name}
                    className="date-picker-field"
                    ref={this.props.dateInput}
                    value={this.props.value}
                    autoComplete="off"
                    onChange={this.props.onChange}
                    onClick={this.props.onClick}
                    type="text" />
        </CustomInputWrap>
      )
    }
}


const CustomInputWrap = styled.div`

    & > input {
        display: block;
        width: 100%;
        border: 1px solid #ddd;
        padding: 5px 12px;
        border-radius: 4px;
    }
`


DatePickerField.defaultProps = {
    visible: true,
    label: 'Date Picker', 
    name: 'date-picker', 
    onChange: () => {}, 
    value: null, 
    error: null  
}

const Container = styled.div `
    margin: 0  0 10px;

    &.has-error {
      color: red;

      input {
        border: 1px solid red;
      }

    }

    .react-datepicker-wrapper,
    .react-datepicker__input-container {
        display: block;
    }
`

const Label = styled.label `
    display: block;
`