import React, { useEffect, useState } from "react";
import styled from 'styled-components';

import * as utils from '../../../utils';
import DatePicker from "react-datepicker";
import InputRange from 'react-input-range';
import moment from 'moment'

import { connect, getIn } from 'formik';
import InputFeedback from '../InputFeedback'

import './CustomDatePicker.scss';

const CustomDatePicker = ({label, name, visible, minYear, maxYear, ...props}) => {

    if (!visible) return null;

    const [date, setDate] = useState();

    const error = getIn(props.formik.errors, name);
    const touch = getIn(props.formik.touched, name);

    let className = touch && error ? 'has-error' : '';
    let errorMsg = touch && error ? error : null;
    const value = props.formik.values[name];

    let uiid = label ? utils.uiid(label) : label;

    const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    const shortMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

    useEffect(() => {
        
        let mounted = true

        if(mounted && value) {
            setDate( new Date(value) )

        };

        // Do Cleanup!...
        return () => {
            mounted = false;
        }

    }, []);


    const handleChange = (date) => {

        setDate(date)

        const d = date ? moment(date).format('MM/DD/YYYY') : '';


        props.formik.setFieldValue(name, d);
        props.formik.setFieldTouched(name, true);
        props.onChange(d); 

    }

    // const today = new Date();
    // const yearToday = today.getFullYear();
    // const years = utils.range(yearToday - 90, yearToday + 40)

    return (
        <div className={`component__custom-date-picker ${className}`}>
                
            <Label htmlFor={uiid}><span>{label}</span></Label>
            <DatePicker
                id={uiid}
                selected={date}
                name={name}
                onChange={handleChange}
                // dateFormat="MM/DD/YYYY"
                renderCustomHeader={({
                    date,
                    changeYear,
                    changeMonth,
                    decreaseMonth,
                    increaseMonth,
                    prevMonthButtonDisabled,
                    nextMonthButtonDisabled
                }) => (
                    <div className="date-picker-head">
                        <div className="year-picker">
                            <div className="picker-head">Year <strong>{date.getFullYear()}</strong></div>

                            {/* if type select */}
                            {/* <select value={date.getFullYear()} onChange={({ target: { value } }) => changeYear(value)}>
                                {years.map(option => (
                                    <option key={option} value={option}>
                                    {option}
                                    </option>
                                ))}
                            </select> */}
                            
                            <div className="year-range-wrap">
                                <div className={`year-toggle minus ${date.getFullYear() > minYear ? '' : 'disabled'}`} onClick={() => {
                                    if(date.getFullYear() > minYear) {
                                        changeYear( date.getFullYear() - 1 )
                                    }
                                }}>-</div>
                                    <InputRange
                                        step={1}
                                        minValue={minYear}
                                        maxValue={maxYear}
                                        disabled={false}
                                        value={ date.getFullYear() }
                                        draggableTrack={false}
                                        formatLabel={()=>{}}
                                        onChange={ value => changeYear(value)} />
                                <div className={`year-toggle plus ${date.getFullYear() < maxYear ? '' : 'disabled'}`} onClick={() => {
                                    if(date.getFullYear() < maxYear) {
                                        changeYear( date.getFullYear() + 1 )
                                    }
                                }}>+</div>
                            </div>
                        </div>

                        <div className="month-picker">
                            <div className="picker-head">Month</div>
                            {
                                shortMonths.map( (m, i) => {
                                    return <Month key={i} selected={date.getMonth() === i} onClick={() => changeMonth(i)}>{m}</Month>
                                })
                            }
                        </div>
                    </div>
                )}
                customInput={<CustomInput />}
            />
            <InputFeedback error={errorMsg}/>

        </div>
    );
}

export default connect(CustomDatePicker);


class CustomInput extends React.Component {
    render () {
      return (
          <CustomInputWrap>
            <input name={this.props.name}
                    className="date-picker-field"
                    value={this.props.value}
                    autoComplete="off"
                    onChange={this.props.onChange}
                    onClick={this.props.onClick}
                    type="text" />
        </CustomInputWrap>
      )
    }
}


const CustomInputWrap = styled.div`
    & > input {
        display: block;
        width: 100%;
        border: 1px solid #ddd;
        padding: 5px 12px;
        border-radius: 4px;
    }
`
const Month = styled.div `
    display: inline-block;
    border: 1px solid #ddd;
    width: 34px;
    height: 34px;
    border-radius: 4px;
    margin: 0 2px 2px 0;
    line-height: 30px;
    cursor: pointer;
    background: ${props => props.selected ? 'white' : 'transparent'}
`

const Label = styled.label `
    display: block;
`

const today = new Date();
const yearToday = today.getFullYear();

CustomDatePicker.defaultProps = {
    visible: true,
    label: 'Date Picker', 
    name: 'date-picker', 
    onChange: () => {}, 
    value: null, 
    error: null,
    minYear: yearToday - 90,
    maxYear: yearToday + 30,
}