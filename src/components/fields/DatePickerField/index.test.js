import React from 'react'
import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
configure({adapter: new Adapter() });

import DatePickerField from './index.js'

const date = shallow(<DatePickerField/>);

it('DatePickerField should match snapshots', () => {
    expect(date).toMatchSnapshot()
})