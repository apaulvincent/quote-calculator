import React from "react";
import styled from 'styled-components';
import * as utils from '../../../utils';

import { connect, getIn } from 'formik';
import InputFeedback from '../InputFeedback'

const TextInput = ({label, name, visible, type, placeholder, ...props}) => {

    if (!visible) return null;

    let uiid = utils.uiid(label);

    const error = getIn(props.formik.errors, name);
    const touch = getIn(props.formik.touched, name);

    let className = touch && error ? 'has-error' : '';
    let errorMsg = touch && error ? error : null;
    const value = props.formik.values[name];

    const handleChange = e => {

        let value = e.nativeEvent.target.value;
  
        props.formik.setFieldValue(name, value);
        props.formik.setFieldTouched(name, true);
        props.onChange(value); 
    };

    return (
        <Container className={className}>

            <Label htmlFor={uiid}><span>{label}</span></Label>

            <FieldWrap fieldType={type}>
                <input id={uiid} 
                        autoComplete="off"
                        type="text" name={name} value={value} onChange={handleChange} placeholder={placeholder}/>
            </FieldWrap>

            <InputFeedback error={errorMsg}/>
        </Container>
    );
}

export default connect(TextInput)

TextInput.defaultProps = {
    visible: true,
    type: 'text', // money, number, percent, phone?
    label: 'Input field',
    placeholder: '',
    onChange: () => {},
}


const Label = styled.label `
    display: block;
`

const defaultStyle = `
    display: block;
    width: 100%;
    border: 1px solid #ddd;
    padding: 5px 12px;
    border-radius: 4px;

    &::placeholder {
        color: #ccc;
        font-style: italic;
    }
`

const Container = styled.div `
    margin: 0  0 10px;

    &.has-error {
      color: red;

      input {
        border: 1px solid red;
      }
    }

    & input {
        ${defaultStyle}
    }
`

const money = `
    position: relative;

    & > input {
        padding-left: 20px;
        padding-right: 32px;
        text-align: right;
    }

    &:before {
        content: '$';
        position: absolute;
        top: 0;
        left: 10px;
        font-size: 12px;
        line-height: 36px;
        vertical-align: middle;
    }

    &:after {
        content: '.00';
        position: absolute;
        top: 0;
        right: 10px;
        font-size: 11px;
        line-height: 36px;
        vertical-align: middle;
    }
`

const percent = `
    position: relative;

    & > input {
        padding-left: 20px;
        padding-right: 32px;
        text-align: right;
    }

    &:after {
        content: '%';
        position: absolute;
        top: 0;
        right: 10px;
        font-size: 11px;
        line-height: 36px;
        vertical-align: middle;
    }
`


const FieldWrap = styled.div(props => {

    switch(props.fieldType) {
        case 'money':
            return money;
        break
        case 'percent':
            return percent;
        break
    }

})




