import React from 'react'

// NOTE:  Enzyme 3 Requires config to work...
import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
configure({adapter: new Adapter() });
// 

import TextField from './index.js'

const field = shallow(<TextField/>);

it('TextField should match snapshots', () => {
    expect(field).toMatchSnapshot()
})