import React, { useState } from "react";
import styled from 'styled-components'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actionCreators from '../store/actions';

import Results from './Results'
import AssetDetails from './forms/AssetDetails'
import FinanceDetails from './forms/FinanceDetails'

import { withFormik } from 'formik';
import * as Yup from 'yup';


const schema = ({fields}) => {

    let schemaObj = {}

    fields.forEach( field => {

        if(field.name === 'pament_terms') {
        
            schemaObj[field.name] = Yup.string().when("loan_product", {
                                is: 'Commercial',
                                then: Yup.string().required(`${field.label} is required.`),
                            });
            return;
        }

        if(field.name === 'business_income') {
        
          schemaObj[field.name] = Yup.string().when("loan_product", {
                              is: 'Commercial',
                              then: Yup.string().required(`${field.label} is required.`),
                          });
          return;
      }

        if(field.name === 'sub_asset_type') {
        
          schemaObj[field.name] = Yup.string().when("asset_type", {
                              is: (val) => {
                                return ['Heavy Vehicles', 'Leisure Goods', 'Other Equipment'].indexOf(val) !== -1
                              },
                              then: Yup.string().required(`${field.label} is required.`),
                          });
          return;
      }

      if(field.name === 'nvic') {
        
          schemaObj[field.name] = Yup.string().when("asset_type", {
                              is: 'Motor Vehicle',
                              then: Yup.string().required(`${field.label} is required.`),
                          });
          return;
      }


        schemaObj[field.name] = Yup.string().required(`${field.label} is required.`)

    });

    return Yup.object().shape(schemaObj);

}

// NOTE: Here we can include Record Level Validation.
const validate = (values, props) => {

  let errors = {};

  return errors;

};

const enhancer = withFormik({

    validationSchema: schema,
    validate: validate,
    // validateOnChange: false,
    // validateOnBlur: false,
    
    // NOTE: Must add initialValues from PROPS when using withFormik HOC to prevent field error.
    mapPropsToValues: ({fields}) => {

        let obj = {};

        for (let i = 0; i < fields.length; i++) {
            let item = fields[i];
            obj[item.name] = item.value;
        }
        return obj
    },

    handleSubmit: (payload, { setSubmitting }) => {
      setSubmitting(false);
    },

    // enableReinitialize: true,
    displayName: 'Calculator',
});

const Calculator = (props) => {

  const [currentTab, setCurrentTab] = useState('financial');

  const toggleTab = (val) => (e) => {
    setCurrentTab(val)
  }

    return (

        <Container>
            <FilterWrap>
                <form onSubmit={props.handleSubmit} >
                    <TabToggle>
                      <h3 className={currentTab === 'financial' ? 'active' : ''} onClick={toggleTab('financial')}>Financial Details</h3>
                      <h3 className={currentTab === 'asset' ? 'active' : ''} onClick={toggleTab('asset')}>Asset Details</h3>
                    </TabToggle>
                    <PanelWrap>
                      <TabPanel className={currentTab === 'financial' ? 'active' : ''}>
                        <FinanceDetails {...props} extraData={currentTab}/>
                      </TabPanel>
                      
                      <TabPanel className={currentTab === 'asset' ? 'active' : ''}>
                        <AssetDetails {...props}/>
                      </TabPanel>
                      <br/>
                      <button type="submit" disabled={props.isSubmitting}>Submit</button>
                    </PanelWrap>
                </form>
           </FilterWrap>
           <ResultsWrap>
                <Results/>
                <DisplayFormState {...props}/>
          </ResultsWrap>
        </Container>

    );
}

const mapStateToProps = state => ({
    ...state.filterReducer,
})

export default connect(mapStateToProps)(enhancer(Calculator));

const PanelWrap = styled.div `
  padding: 20px;
`

const TabToggle = styled.div `
  display: flex;
  flex-wrap: nowrap;
  flex-direction: row;

    h3 {
      flex: 1 0 50%;
      text-align: center;
      padding: 14px;
      cursor: pointer;
      background: #ddd;
    }

    h3.active {
      background: transparent;
    }
`

const TabPanel = styled.div `
    display: none;

    &.active {
      display: block;
    }
`


const Container = styled.div `
    display: flex;
    flex-wrap: nowrap;
    flex-direction: row;
    height: 100%;
`

const FilterWrap = styled.div `
  flex: 0 0 30%;
  border: 1px solid #ddd;
  height: 100%;
  overflow: auto;
`

const ResultsWrap = styled.div `
  flex: 1 0 70%;
  border: 1px solid #ddd;
  padding: 20px;
  height: 100%;
  overflow: auto;
`


const DisplayFormState = ({fields,history,location,match, ...props}) =>
  <div style={{ margin: '1rem 0' }}>
    <h3 style={{ fontFamily: 'monospace' }} />
    <pre
      style={{
        background: '#f6f8fa',
        fontSize: '.65rem',
        padding: '.5rem',
      }}
    >
      <strong>FORM</strong> = {JSON.stringify(props, null, 2)}
    </pre>
</div>;