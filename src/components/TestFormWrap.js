import React from "react";
import styled from 'styled-components'

import { connect } from 'react-redux';


import TestForm from './forms/TestForm'
import Results from './Results'

import { withFormik } from 'formik';
import * as Yup from 'yup';


const schema = ({fields}) => {

    let schemaObj = {}

    fields.forEach( field => {

        if(field.name === 'pament_terms') {
        
            schemaObj[field.name] = Yup.string().when("loan_product", {
                                is: 'Commercial',
                                then: Yup.string().required(`${field.label} is required.`),
                            });
            return;
        }

        schemaObj[field.name] = Yup.string().required(`${field.label} is required.`)

    });

    return Yup.object().shape(schemaObj);

}

// NOTE: Here we can include Record Level Validation.
const validate = (values, props) => {

  let errors = {};

  if (values.start_date && values.end_date) {

    const start = new Date(values.start_date).getFullYear();
    const end = new Date(values.end_date).getFullYear();
    const total = end - start;

    if(total < 3) {
        errors.start_date = '3 years or more address history';
        errors.end_date = '3 years or more address history';
    }

  }

  return errors;

};

const enhancer = withFormik({

    validationSchema: schema,
    validate: validate,
    // validateOnChange: false,
    // validateOnBlur: false,
    
    // NOTE: Must add initialValues from PROPS when using withFormik HOC to prevent field error.
    mapPropsToValues: ({fields}) => {
        let obj = {};

        for (let i = 0; i < fields.length; i++) {
            let item = fields[i];
            obj[item.name] = item.value;
        }
        return obj
    },

    handleSubmit: (payload, { setSubmitting }) => {
      setSubmitting(false);
    },

    // enableReinitialize: true,
    displayName: 'TestFormWrap',
});

const TestFormWrap = (props) => {

    return (

        <Container>
            <FilterWrap>
                <h2>Test Fields</h2>
                <form onSubmit={props.handleSubmit} >
                    <TestForm {...props}/>
                    <br/>
                    <button type="submit" disabled={props.isSubmitting}>Submit</button>
                </form>
           </FilterWrap>
           <ResultsWrap>
                <Results/>
                <DisplayFormState {...props}/>
          </ResultsWrap>
        </Container>

    );
}

const mapStateToProps = state => ({
    ...state.testFilterReducer,
})

export default connect(mapStateToProps)(enhancer(TestFormWrap));

const Container = styled.div `
    display: flex;
    flex-wrap: nowrap;
    flex-direction: row;
    height: 100%;
`

const FilterWrap = styled.div `
  flex: 0 0 400px;
  padding: 20px;
  border: 1px solid #ddd;
  height: 100%;
  overflow: auto;
`

const ResultsWrap = styled.div `
  flex: 1 0 calc(100% - 400px);
  border: 1px solid #ddd;
  padding: 20px;
  height: 100%;
  overflow: auto;
`

const DisplayFormState = ({fields,history,location,match, ...props}) =>
  <div style={{ margin: '1rem 0' }}>
    <h3 style={{ fontFamily: 'monospace' }} />
    <pre
      style={{
        background: '#f6f8fa',
        fontSize: '.65rem',
        padding: '.5rem',
      }}
    >
      <strong>FORM</strong> = {JSON.stringify(props, null, 2)}
    </pre>
</div>;