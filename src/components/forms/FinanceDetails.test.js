import React from 'react'
import {shallow, render, mount, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';

import FinanceDetails from './FinanceDetails.js'

configure({adapter: new Adapter() });

describe('FinanceDetails',  () => {

    // it('FinanceDetails should match snapshots', () => {
    //     expect(finance).toMatchSnapshot()
    // });

    it('allows us to set props', () => {
        const wrapper = render(<FinanceDetails bar="baz" />);

        expect(wrapper.props().bar).to.equal('baz');
        wrapper.setProps({ bar: 'foo' });
        expect(wrapper.props().bar).to.equal('foo');
    });
 


});