import React from "react";
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actionCreators from '../../store/actions';

import ButtonGroup from '../fields/ButtonGroup'
import TextInput from '../fields/TextInput'
import DropDown from '../fields/DropDown'
import RangeSlider from '../fields/RangeSlider'

const FinanceDetails = ({fields, values, ...props}) => {

    const getOptions = (name) => {

        switch(name) {
            case 'loan_product':
                return [{
                    value: 'Commercial',
                    label: 'Commercial'
                  },
                  {
                    value: 'Consumer',
                    label: 'Consumer'
              }];
            case 'pament_terms':
                return ['In Arrears', 'In Advance'];
        }
    }

    // NOTE: Conditionally changing values for conditional fields
    const handleLoanProductChange = value => {

        props.setValues({
            ...values,
            loan_product: value,
            pament_terms: '',
            business_income: ''
        });

        // props.setFieldValue('pament_terms', '');
        // props.setFieldValue('business_income', '');

    }

    const nameToLabel = {};

    fields.forEach(field => {
        nameToLabel[field.name] = field.label;
    });

    const residentialStatusOptions = [
        'Mortgage',
        'Own Outright',
        'Renter Agent',
        'Renter Private',
        'Boarder',
        'Living with Parents',
        'Employer Provided',
    ];
    const empTypeOptions = [
        'Full Time',
        'Part Time',
        'Casual',
        'Self Employed - Sole Trader',
        'Self Employed - Company',
    ];
    const creditHistoryOptions = [
        'No Infringements',
        'Paid Infringements',
        'Unpaid Infringements',
    ];
    const businessIncomeOptions = [
        'Full Documentation',
        'Self Declaration',
    ];

    return (
        <Container>
                
                <ButtonGroup 
                            onChange={handleLoanProductChange}
                            label={nameToLabel['loan_product']}
                            name={'loan_product'} 
                            options={getOptions('loan_product')}
                            />

                <TextInput 
                            label={nameToLabel['amount_required']}
                            name={'amount_required'} 
                            type={'money'}
                            placeholder={'Placeholder...'}
                            // Set Touche Manually
                            // onKeyUp={() => props.setFieldTouched('amount_required', true)}
                            />

                <RangeSlider 
                            label={nameToLabel['term_requested']}
                            name={'term_requested'}
                            step={12}
                            min={12}
                            max={84}
                            //extraData={props.extraData} // Pass extraData to re-render component
                            formatLabel={value => (<span><strong>{value}</strong> Months</span>) }
                            />

                <TextInput 
                            label={nameToLabel['baloon']}
                            name={'baloon'}
                            type={'percent'}
                            placeholder={'Placeholder...'}
                            />

                <ButtonGroup 
                            label={nameToLabel['pament_terms']}
                            name={'pament_terms'} 
                            options={getOptions('pament_terms')}
                            visible={values.loan_product === 'Commercial'}
                            />
                
                <br/>
                <h3>Primary Applicant Details</h3>
                <br/>

                <DropDown 
                        options={residentialStatusOptions}
                        label={nameToLabel['residential_status']}
                        name={'residential_status'}
                        />

                <DropDown 
                        options={empTypeOptions}
                        label={nameToLabel['employment_type']}
                        name={'employment_type'}
                        />

                <DropDown 
                        options={creditHistoryOptions}
                        label={nameToLabel['credit_history']}
                        name={'credit_history'}
                        />

                <DropDown 
                        options={businessIncomeOptions}
                        label={nameToLabel['business_income']}
                        name={'business_income'}
                        visible={values.loan_product === 'Commercial'}
                        />

        </Container>
    );
}

const mapStateToProps = state => ({
    ...state.filterReducer,
})

const mapDispatchToProps = dispatch => {
	return bindActionCreators(actionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FinanceDetails);

const Container = styled.div `
`