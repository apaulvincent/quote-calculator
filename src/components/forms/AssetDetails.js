import React from "react";
import styled from 'styled-components';

import DropDown from '../fields/DropDown'
import TextInput from '../fields/TextInput'
import ButtonGroup from '../fields/ButtonGroup'
import CustomDatePicker from '../fields/DatePickerField/CustomDatePicker'

import '../../../node_modules/react-datepicker/dist/react-datepicker.css';

export default function AssetDetails({fields, values, ...props}) {
    

    const selectOptions = [
        'Motor Vehicle',
        'Leisure Goods',
        'Heavy Vehicles',
        'Agricultural Machinery',
        'Yellow Goods',
        'Other Equipment',
    ];

    let select2Options = [];

    if(values.asset_type === 'Leisure Goods') {

        select2Options = [
            'Motorbike - Road Registered',
            'Trail Bike - Unregistered',
            'Boat - Trailered',
            'Boat - Moared',
            'Jetski',
            'Caravan',
            'Camper Trailer',
            'Motorhome',
            'Other Leisure',
        ];

    } else if (values.asset_type === 'Heavy Vehicles') {
        
        select2Options = [
            'Prime Mover',
            'Rigid Truck > 4.5t',
            'Trailer (truck)',
            'Bus',
            'Crane',
        ];

    } else {
        // Other
        select2Options = [
            'Industrial Plant - Lathes, Presses, etc',
            'Material Handling - Fork Lifts, Scissor Lifts, etc',
            'Medical Equipment',
            'Office Equipment - Copiers, Printers, etc',
            'Computer Hardware',
            'POS Systems',
            'Solar Equipment',
            'Other - Not Listed',
        ];

    }

    const select3Options = [
        'Dealer',
        'Private Sale',
        'Sale & Buy Back',
        'Refinance',
    ];

    const nameToLabel = {};
    const recurringFieldIds = [];

    fields.forEach(field => {
        nameToLabel[field.name] = field.label;

        if( field.name.indexOf('baloon_') !== -1 ) {
            let id = field.name.split('_').pop();
            recurringFieldIds.push(id)
        }
    });

    return (
        <Container>

                <DropDown 
                        options={selectOptions}
                        label={nameToLabel['asset_type']}
                        name={'asset_type'}
                        />

                <DropDown 
                        options={select2Options}
                        label={nameToLabel['sub_asset_type']}
                        name={'sub_asset_type'}
                        visible={['Heavy Vehicles', 'Leisure Goods', 'Other Equipment'].indexOf(values.asset_type) !== -1}
                        />

                <DropDown 
                        options={select3Options}
                        label={nameToLabel['vendor_type']}
                        name={'vendor_type'}
                        />

                <ButtonGroup 
                            label={nameToLabel['vehicle_condition']}
                            name={'vehicle_condition'} 
                            options={['New', 'Used']}
                            />

                <CustomDatePicker 
                        label={nameToLabel['year']}
                        name={'year'}
                        />


                <TextInput 
                            label={nameToLabel['nvic']}
                            name={'nvic'} 
                            visible={ 'Motor Vehicle' === values.asset_type}
                            />
        </Container>
    );
}

const Container = styled.div `
`