import React, { useState } from "react";
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actionCreators from '../../store/actions';

import ButtonGroup from '../fields/ButtonGroup'
import DropDown from '../fields/DropDown'
import TextInput from '../fields/TextInput'
import DatePickerField from '../fields/DatePickerField'
import CustomDatePicker from '../fields/DatePickerField/CustomDatePicker'

import posed, { PoseGroup } from 'react-pose';

import '../../../node_modules/react-datepicker/dist/react-datepicker.css';

const TestForm = ({fields, values, ...props}) => {

    const [recurringCount, setRecurringCount] = useState(1000);

    const selectOptions = [
        {value: 'Motor Vehicle', label: 'Motor Vehicle'},
        {value: 'Leisure Goods', label: 'Leisure Goods'},
        {value: 'Heavy Vehicles', label: 'Heavy Vehicles'},
        {value: 'Agricultural Machinery', label: 'Agricultural Machinery'},
        {value: 'Yellow Goods', label: 'Yellow Goods'},
        {value: 'Other Equipment', label: 'Other Equipment'},
    ];

    const nameToLabel = {};
    const recurringFieldIds = [];

    fields.forEach(field => {
        nameToLabel[field.name] = field.label;

        if( field.name.indexOf('baloon_') !== -1 ) {
            let id = field.name.split('_').pop();
            recurringFieldIds.push(id)
        }
    });

    const getOptions = (name) => {

        switch(name) {
            case 'loan_product':
                return [{
                    value: 'Commercial',
                    label: 'Commercial'
                  },
                  {
                    value: 'Consumer',
                    label: 'Consumer'
              }];
            break;
            case 'pament_terms':
                return ['In Arrears', 'In Advance'];
            break;
        }
    }

    // NOTE: Conditionally changing values for conditional fields
    const handleLoanProductChange = value => {

        props.setValues({
            ...values,
            loan_product: value,
            pament_terms: ''
        });
    }

    const handleAdd = (e) => {

        e.preventDefault();
        
        const newFields = [{
                            label: 'Baloon',
                            name: 'baloon_' + recurringCount,
                            required: true,
                            value: '0.2',
                        },
                        {
                            label: 'Date',
                            name: 'date_' + recurringCount,
                            required: true,
                            value: '',
                        },
                        {
                            label: 'Term Requested',
                            name: 'term_requested_' + recurringCount,
                            required: true,
                            value: '3',
                        }];
        
        let newVal = values;

        newFields.forEach(f => {
            newVal[f.name] = f.value
        });

        props.setValues({
            ...newVal,
        });

        // Save to global state...
        props.addFields(newFields)

        setRecurringCount(recurringCount + 1)
    };

    const handleDelete = (id) => (e) => {

        const keys = ['baloon_'+ id, 'date_'+ id,  'term_requested_'+ id ];

        let newVal = values;
        
        Object.keys(newVal).forEach( v => {

            if( keys.indexOf(v) !== -1 ) {
                delete newVal[v]
            }
        });

        props.setValues({
            ...newVal,
        });

        props.removeFields(keys)

    };




    return (
        <Container>

                <span>Payment Terms</span>
                <h3>Conditional Field & Validation</h3>

                <ButtonGroup 
                            onChange={handleLoanProductChange}
                            label={nameToLabel['loan_product']}
                            name={'loan_product'} 
                            value={values['loan_product']}
                            options={getOptions('loan_product')}
                            {...props}/>

                <TextInput 
                            label={nameToLabel['amount_required']}
                            name={'amount_required'} 
                            />

                <ButtonGroup 
                            label={nameToLabel['pament_terms']}
                            name={'pament_terms'} 
                            value={values['pament_terms']}
                            options={getOptions('pament_terms')}
                            visible={values.loan_product === 'Commercial'}
                            {...props}/>
                
                <TextInput 
                            label={nameToLabel['residential_status']}
                            name={'residential_status'}
                            />
                
                <br/>
                <h3>Recuring Fields</h3>

                <CardGroup>
                    <PoseGroup>
                    {
                        /* Recurring Fields */
                        recurringFieldIds.map((id, i) => {

                        return  <Card className="card" key={i}>

                                    <span className="delete" onClick={handleDelete(id)}>X</span>

                                    <TextInput 
                                            label={nameToLabel['baloon_'  + id]}
                                            name={'baloon_'  + id}
                                            />

                                    <DatePickerField 
                                            onChange={props.handleChange} 
                                            label={nameToLabel['date_' + id]}
                                            name={'date_' + id}
                                            value={values['date_' + id]}
                                            error={props.touched['date_' + id] && props.errors['date_' + id]}/>

                                    <TextInput 
                                            label={nameToLabel['term_requested_' + id]}
                                            name={'term_requested_' + id}
                                            />
                                </Card>
                        })
                    }
                    </PoseGroup>
                </CardGroup>

                <AddButton>
                    <button onClick={handleAdd}>Add Fields</button>
                </AddButton>

                <DropDown 
                        options={selectOptions}
                        label={nameToLabel['asset_type']}
                        name={'asset_type'}
                        />

                <TextInput 
                        label={nameToLabel['year']}
                        name={'year'}
                        />

                <br/>
                <span>3 Years Or More History</span>
                <h3>Record Level Validation </h3>

                <CustomDatePicker 
                        label={nameToLabel['start_date']}
                        name={'start_date'}
                        />

                <CustomDatePicker 
                        label={nameToLabel['end_date']}
                        name={'end_date'}
                        />

        </Container>
    );
}

const Card = posed.div({
    enter: {
        opacity: 1,
        scale: 1,
        type: 'physics',
        transition: { duration: 200 }
    },
    exit: {
        opacity: 0,
        scale: 0.9,
        type: 'physics',
        transition: { duration: 100 }
    }
});

const mapStateToProps = state => ({
    ...state.testFilterReducer,
})

const mapDispatchToProps = dispatch => {
	return bindActionCreators(actionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TestForm);


const Container = styled.div `
`
const AddButton = styled.div `
    text-align: right;
`

const CardGroup = styled.div `

    .card {
        position: relative;
        border: 1px solid #ddd;
        background: #fafafa;
        border-radius: 4px;
        padding: 20px;
        margin: 15px 0;
    }

    span.delete {
        position: absolute;
        top: 10px;
        right: 20px;
        font-weight: bold;
        cursor: pointer;
    }
`
