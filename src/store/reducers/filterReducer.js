import * as actionTypes from '../actions/types'

let defaultData = {
	fields: [{
			  label: 'Loan Product',
			  name: 'loan_product',
			  required: true,
			  value: 'Commercial',
			},
			{
			  label: 'Amount Required',
			  name: 'amount_required',
			  required: true,
			  value: '1000',
			},
			{
			  label: 'Term Requested',
			  name: 'term_requested',
			  required: true,
			  value: 24,
			},
			{
			  label: 'Baloon',
			  name: 'baloon',
			  required: true,
			  value: '',
			},
			{
			  label: 'Payment Terms',
			  name: 'pament_terms',
			  required: true,
			  value: '',
			},
			{
			  label: 'Residential Status',
			  name: 'residential_status',
			  required: true,
			  value: 'Mortgage',
			},
			{
			  label: 'Employment Type',
			  name: 'employment_type',
			  required: true,
			  value: 'Full Time',
			},
			{
			  label: 'Credit History',
			  name: 'credit_history',
			  required: true,
			  value: 'No Infringements',
			},
			{
			  label: 'Business Income Verification Method',
			  name: 'business_income',
			  required: true,
			  value: 'Full Documentation',
			},
			{
			  label: 'Asset Type',
			  name: 'asset_type',
			  required: true,
			  value: 'Motor Vehicle',
			},
			{
			  label: 'Sub Asset Type',
			  name: 'sub_asset_type',
			  required: true,
			  value: '',
			},
			{
			  label: 'Vendor Type',
			  name: 'vendor_type',
			  required: true,
			  value: '',
			},
			{
			  label: 'Vehicle Condition',
			  name: 'vehicle_condition',
			  required: true,
			  value: '',
			},
			{
			  label: 'Year',
			  name: 'year',
			  required: true,
			  value: '',
			},
			{
			  label: 'NVIC',
			  name: 'nvic',
			  required: true,
			  value: '',
			},
			// {
			//   label: 'Start Date',
			//   name: 'start_date',
			//   required: true,
			//   value: '',
			// },
			// {
			//   label: 'End Date',
			//   name: 'end_date',
			//   required: true,
			//   value: '',
			// },
		]
}

const filterReducer = (state = defaultData, action) => {
	switch(action.type){

		case actionTypes.FETCH_DATA:
		return state

		case actionTypes.ADD_FIELDS:

		return {
			...state,
			fields: [
				...state.fields,
				...action.newFields
			]
		}

		case actionTypes.REMOVE_FIELDS:

		return {
			...state,
			fields: state.fields.filter(f => {

				if(action.keys.indexOf(f.name) !== -1) {
					return false
				}

				return true

			})
		}

		default:
		return state;
	}	
}

export default filterReducer;