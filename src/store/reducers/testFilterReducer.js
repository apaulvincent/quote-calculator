import * as actionTypes from '../actions/types'

let defaultData = {
	fields: [{
			  label: 'Loan Product',
			  name: 'loan_product',
			  required: true,
			  value: 'Commercial',
			},
			{
			  label: 'Amount Required',
			  name: 'amount_required',
			  required: true,
			  value: '1000',
			},
			{
			  label: 'Payment Terms',
			  name: 'pament_terms',
			  required: true,
			  value: '',
			},
			{
			  label: 'Residential Status',
			  name: 'residential_status',
			  required: true,
			  value: 'Mortgage',
			},
			{
			  label: 'Asset Type',
			  name: 'asset_type',
			  required: true,
			  value: '',
			},
			{
			  label: 'Year',
			  name: 'year',
			  required: true,
			  value: '',
			},
			{
			  label: 'Start Date',
			  name: 'start_date',
			  required: true,
			  value: '',
			},
			{
			  label: 'End Date',
			  name: 'end_date',
			  required: true,
			  value: '',
			},
		]
}

const testFilterReducer = (state = defaultData, action) => {
	switch(action.type){

		case actionTypes.FETCH_DATA:
		return state

		case actionTypes.ADD_FIELDS:

		return {
			...state,
			fields: [
				...state.fields,
				...action.newFields
			]
		}

		case actionTypes.REMOVE_FIELDS:

		return {
			...state,
			fields: state.fields.filter(f => {

				if(action.keys.indexOf(f.name) !== -1) {
					return false
				}

				return true

			})
		}

		default:
		return state;
	}	
}

export default testFilterReducer;