import { combineReducers } from 'redux'

import filterReducer  from './filterReducer'
import testFilterReducer  from './testFilterReducer'

const rootReducer = combineReducers({
	filterReducer,
	testFilterReducer
});

export default rootReducer;


