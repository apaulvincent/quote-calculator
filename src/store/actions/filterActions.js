import * as actionTypes from './types'

export const fetchData = () => {
    return {
        type: actionTypes.FETCH_DATA,
    }
};

export const addFields = (fields) => {
    return {
        type: actionTypes.ADD_FIELDS,
        newFields: fields
    }
}
export const removeFields = (keys) => {
    return {
        type: actionTypes.REMOVE_FIELDS,
        keys: keys
    }
}
